from django.core.urlresolvers import reverse_lazy

from django.template.response import TemplateResponse

from django.http import HttpResponseForbidden

from django.shortcuts import redirect

class AnonimousRequiredMixin(object):
	# redirect url if user is not anonimous
	non_anonimous_url = None

	# dispatch signal method override
	def dispatch(self, request, *args, **kwargs):
		if request.user and request.user.is_authenticated():
			return redirect(reverse_lazy(self.non_anonimous_url))
		return super(AnonimousRequiredMixin, self).dispatch(request, *args, **kwargs)

class LobbyOpenedRequiredMixin(object):
	# redirect url if lobby is closed - lobby has not null "closed" value
	lobby_access_denied_url = None

	# dispatch signal method override
	def dispatch(self, request, *args, **kwargs):
		instance = self.get_object()
		if instance.closed:
			return redirect(reverse_lazy(self.lobby_access_denied_url))
		return super(LobbyOpenedRequiredMixin, self).dispatch(request, *args, **kwargs)

class LobbyWatingRequiredMixin(object):
	lobby_access_denied_url = None

	def dispatch(self, request, *args, **kwargs):
		instance = self.get_object()
		if not instance.is_wating():
			return redirect(reverse_lazy(self.lobby_access_denied_url))
		return super(LobbyWatingRequiredMixin, self).dispatch(request, *args, **kwargs)
