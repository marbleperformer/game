from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy

from django.contrib.auth import login, logout

from django.contrib.auth.models import User

from django.contrib.sessions.models import Session

from django.views.generic import FormView

from rest_framework import viewsets

from rest_framework import filters

from django.utils import timezone

from .forms import LoginForm, SigninForm

from .serializers import UserSerializer

import mixins

class LoginView(mixins.AnonimousRequiredMixin, FormView):
	template_name = 'login.html'

	success_url = 'lobbies:index'
	non_anonimous_url = 'lobbies:index'

	form_class = LoginForm

	def post(self, request, *args, **kwargs):
		form = self.form_class(request.POST)
		if form.is_valid():
			user = form.login()
			login(request, user)
			return redirect(reverse_lazy(self.success_url))
		return render(request, self.template_name, {'form': form})

class SigninView(mixins.AnonimousRequiredMixin, FormView):
	template_name = 'signin.html'

	success_url = 'lobbies:index'
	non_anonimous_url = 'lobbies:index'

	form_class = SigninForm

	def post(self, request, *args, **kwargs):
		form = self.form_class(request.POST)
		if form.is_valid():
			user = form.save()
			login(request, user)
			return redirect(reverse_lazy(self.success_url))
		return render(request, self.template_name, {'form': form})

class UserViewSet(viewsets.ModelViewSet):
	filter_backends = (filters.DjangoFilterBackend,)
	filter_fields = ('id', 'username')

	queryset = User.objects.all()
	serializer_class = UserSerializer
