from django.test import TestCase, Client
from django.contrib.auth.models import User

# Create your tests here.
class AuthenticationViewTests(TestCase):
	def setUp(self):
		self.clt = Client()
		self.user_data = {'username':'Test', 'password': 'Test123456'}
		User.objects.create_user(**self.user_data)

	def test_login_view(self):
		URL = '/'
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 200)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 200)
		login = self.clt.login(**self.user_data)
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 302)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 302)

	def test_signin_view(self):
		URL = '/signin'
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 200)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 200)
		login = self.clt.login(**self.user_data)
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 302)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 302)
