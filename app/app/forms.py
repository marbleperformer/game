import datetime
from django import forms

from django.contrib.auth import authenticate

from django.contrib.auth.models import Group, User
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from django.utils import timezone

class LoginForm(forms.Form):
	username = forms.CharField(label='Username')
	password = forms.CharField(label='Password', widget=forms.PasswordInput)

	def clean(self):
		username = self.cleaned_data.get('username')
		password = self.cleaned_data.get('password')
		user = authenticate(username=username, password=password)
		if not user:
			raise forms.ValidationError('Wrong password or login.')
		if not user.is_active:
			raise forms.ValidationError('For this user access to the system is limited.')
		return self.cleaned_data

	def login(self):
		username = self.cleaned_data.get('username')
		password = self.cleaned_data.get('password')
		user = authenticate(username=username, password=password)
		return user

class SigninForm(forms.ModelForm):
	password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
	password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

	class Meta:
		model = User
		fields = ['username', 'password1', 'password2']

	def clean_password2(self):
		password1 = self.cleaned_data.get('password1')
		password2 = self.cleaned_data.get('password2')

		if password1 and password2 and password1 != password2:
			raise forms.ValidationError('Passwords does not mutch.')

		return password2

	def save(self, commit=True):
		user = super(SigninForm, self).save(commit=False)
		user.set_password(self.cleaned_data['password1'])
		if commit:
			user.save()
		return user