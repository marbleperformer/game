import json

from collections import OrderedDict

from tornado import websocket

# keeps channel_name - user_id pairs
channels = OrderedDict()
# keeps user_id - channel_name pairs
sockets = OrderedDict()
# keeps user_id - socket pairs
users = OrderedDict()

class MainMessageHandler(websocket.WebSocketHandler):
	# eliminates the need for authentication
	def check_origin(self, origin):
		return True

	def open(self, user_id):
		self.user_id = user_id 
		users[user_id] = self
		sockets[user_id] = set()

	def on_message(self, message):
		message_json = json.loads(message, object_pairs_hook=OrderedDict)
		# set message sender id
		message_json['identity'] = self.user_id
		if message_json['method'] == 'OPEN':
			if not message_json['channel'] in channels:
				channels[message_json['channel']] = set()

			message_json['body'] = {'identity':self.user_id}

			message_json['body'] = {'identity':self.user_id}
			for user_id in channels[message_json['channel']]:
				users[user_id].write_message(json.dumps(message_json))

			message_json['method'] = 'INIT'
			message_json['identity'] = self.user_id

			message_json['body'] = {'online_users':list(users.keys())}

			self.write_message(json.dumps(message_json))

			channels[message_json['channel']].add(self.user_id)
			sockets[self.user_id].add(message_json['channel'])

		elif message_json['method'] == 'CLOSE':
			return;
		else:
			for user_id in channels[message_json['channel']]:
				users[user_id].write_message(json.dumps(message_json))

	def on_close(self):
		message_json = OrderedDict({
			'method':'CLOSE',
			'identity':self.user_id,
			'body':{'identity':self.user_id}
		})
		for channel in sockets[self.user_id]:
			message_json['channel'] = channel
			for user_id in channels[channel]:
				if not users[user_id] is self:
					users[user_id].write_message(message_json)
			channels[channel].remove(self.user_id)
		del(users[self.user_id])
