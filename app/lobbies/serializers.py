from rest_framework.serializers import ModelSerializer, HyperlinkedRelatedField, StringRelatedField

from django.contrib.auth.models import User

from .models import Lobby, Hint, Turn

class LobbySerializer(ModelSerializer):
	class Meta:
		model = Lobby
		fields = (
			'url', 'id', 'name', 'timer', 'owner', 'winner',
			'status', 'players', 'created', 'closed'
		)

class HintSerializer(ModelSerializer):
	player = HyperlinkedRelatedField(view_name='user-detail', read_only=True)

	class Meta:
		model = Hint
		fields = ('url', 'id', 'player', 'lobby', 'created')

class TurnSerializer(ModelSerializer):
	player = HyperlinkedRelatedField(view_name='user-detail', read_only=True)

	class Meta:
		model = Turn
		fields = ('url', 'id', 'player', 'figure', 'lobby', 'created')
