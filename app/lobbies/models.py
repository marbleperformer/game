from django.db import models

from django.core.validators import MaxValueValidator

class Lobby(models.Model):
	WAITING='WTN'
	READY='RED'
	INPROCESS='PRC'
	STATUS_LIST = (
		(WAITING, 'Waiting for players'),
		(READY, 'Ready to start'),
		(INPROCESS, 'Game in process')
	)

	name = models.CharField(max_length=150)

	timer = models.PositiveIntegerField(default=10, validators=[MaxValueValidator(300),])

	status = models.CharField(max_length=3, choices=STATUS_LIST, default=WAITING)

	players = models.ManyToManyField('auth.User', related_name='game_sessions', blank=True)

	owner = models.ForeignKey('auth.User', related_name='lobbies')

	winner = models.ForeignKey('auth.User', related_name='victories', null=True, blank=True)

	created = models.DateTimeField(auto_now_add=True)
	closed = models.DateTimeField(blank=True, null=True)

	class Meta:
		verbose_name = 'Lobby'
		verbose_name_plural = 'Lobbies'

	def __str__(self):
		return self.name

	def save(self):
		super(Lobby, self).save()
		self.players.add(self.owner)

	def is_wating(self):
		return self.players.count() < 2

class Action(models.Model):
	player = models.ForeignKey('auth.User', related_name='player')

	lobby = models.ForeignKey(Lobby, related_name='actions', on_delete=models.CASCADE)

	tour = models.IntegerField(default=0)

	created = models.DateTimeField(auto_now_add=True)

class Hint(Action):
	def __str__(self):
		return '/'.join((self.lobby.name, self.player.username))

class Turn(Action):
	ROCK_FIGURE = 'ROC'
	PAPER_FIGURE = 'PAP'
	SCISSORS_FIGURE = 'SCI'
	LIZARD_FIGURE = 'LIZ'
	SPOCK_FIGURE = 'SPC'

	FIGURE_LIST = [
		(ROCK_FIGURE, 'Rock'),
		(PAPER_FIGURE, 'Paper'),
		(SCISSORS_FIGURE, 'Scissors'),
		(LIZARD_FIGURE, 'Lizard'),
		(SPOCK_FIGURE, 'Spock'),
	]

	figure = models.CharField(max_length=3, choices=FIGURE_LIST, blank=True, null=True)	

	def __str__(self):
		return '/'.join((self.lobby.name, self.player.username, str(self.figure)))
