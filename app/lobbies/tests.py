from django.test import TestCase, Client

from django.contrib.auth.models import User

from django.utils import timezone

from lobbies.models import Lobby

class LobbyViewTests(TestCase):
	def setUp(self):
		self.clt = Client()
		self.user_data = {'username':'Test', 'password': 'Test123456'}
		user = User.objects.create_user(**self.user_data)
		self.lobby_data = {'name': 'Test', 'owner': user}
		self.lobby = Lobby(**self.lobby_data)
		self.lobby.save()

	def test_list_view(self):
		URL = '/lobbies/'
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 302)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 302)
		login = self.clt.login(**self.user_data)
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 200)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 405)

	def test_detail_view(self):
		URL = '/lobbies/detail/' + str(self.lobby.id)
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 302)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 302)
		login = self.clt.login(**self.user_data)
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 200)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 405)

	def test_list_view(self):
		URL = '/lobbies/create'
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 302)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 302)
		login = self.clt.login(**self.user_data)
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 200)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 200)

	def test_detail_view(self):
		URL = '/lobbies/detail/' + str(self.lobby.id)
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 302)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 302)
		login = self.clt.login(**self.user_data)
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 200)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 405)

	def test_playground_view(self):
		URL = '/lobbies/playground/' + str(self.lobby.id)
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 302)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 302)
		login = self.clt.login(**self.user_data)
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 200)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 405)

	def test_lobby_closed_playground_view(self):
		new_lobby_data = self.lobby_data.copy()
		new_lobby_data.update(closed=timezone.now())
		new_lobby = Lobby(**new_lobby_data)
		new_lobby.save()

		URL = '/lobbies/playground/' + str(new_lobby.id)
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 302)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 302)
		login = self.clt.login(**self.user_data)
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 302)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 302)

	def test_lobby_full_playground_view(self):
		URL = '/lobbies/playground/' + str(self.lobby.id)

		new_user_data = {'username':'Test1', 'password': 'Test123456'}
		new_user = User.objects.create_user(**new_user_data)
		self.lobby.players.add(new_user)

		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 302)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 302)
		login = self.clt.login(**self.user_data)
		response = self.clt.get(URL)
		self.assertEqual(response.status_code, 302)
		response = self.clt.post(URL)
		self.assertEqual(response.status_code, 302)
