import json
from collections import OrderedDict
from tornado import websocket, httpclient
import tornadoredis

from django.utils import timezone
from django.contrib.auth.models import User

from .models import Lobby, Hint, Turn

import urllib

online_users = OrderedDict()

SEND_HINT_MESSAGE_TYPE = 'SHM'
SEND_FIGURE_MESSAGE_TYPE = 'SFM'
SEND_CLOSE_LOBBY_TYPE = 'SCL'
SEND_START_GAME_TYPE = 'SSG'
SEND_USER_ENTER_TYPE = 'SUE'

class LobbyActionHandler(websocket.WebSocketHandler):
	def check_origin(self, origin):
		return True

	def handle_request(self, response):
		print(response)

	def send_hint_message(self):
		# client = httpclient.AsyncHTTPClient()
		# request = httpclient.HTTPRequest(
		# 	'/api/hints/',
		# 	method = 'PUT',
		# 	body={
		# 		'player':self.user,
		# 		'lobby':self.lobby,
		# 	}
		# )
		# client.fetch(request, self.handle_request)

		message = {
			'type':SEND_HINT_MESSAGE_TYPE,
			'user_id':self.user_id,
			'lobby_id':self.lobby_id
		}
		for sock in list(online_users.values()):
			sock.write_message(json.dumps(message))

	def send_figure_message(self, figure):
		# client = httpclient.AsyncHTTPClient()
		# request = httpclient.HTTPRequest(
		# 	'/api/turns/',
		# 	method='PUT',
		# 	body={
		# 		'player':self.user,
		# 		'lobby':self.lobby,
		# 		'figure':figure,
		# 	}
		# )
		# client.fetch(request, self.handle_request)

		message = {
			'type':SEND_FIGURE_MESSAGE_TYPE,
			'user_id':self.user_id,
			'lobby_id':self.lobby_id,
			'figure':figure
		}
		for sock in list(online_users.values()):
			sock.write_message(json.dumps(message))

	def send_start_game(self):
		message = {
			'type':SEND_START_GAME_TYPE,
			'lobby_id':self.lobby_id
		}
		for sock in list(online_users.values()):
			sock.write_message(json.dumps(message))

	def send_user_enter(self, username):
		message = {
			'type':SEND_USER_ENTER_TYPE,
			'username':username,
			'lobby_id':self.lobby_id
		}
		for sock in list(online_users.values()):
			sock.write_message(json.dumps(message))

	def send_close_lobby(self):
		# client = httpclient.AsyncHTTPClient()
		# request = httpclient.HTTPRequest(
		# 	url='http://127.0.0.1:8000/api/lobbies/%s/' % self.lobby_id,
		# 	method='PUT',
		# 	body=urllib.parse.urlencode({
		# 		'closed':timezone.now()
		# 	})
		# )
		# client.fetch(request, self.handle_request)

		message = {
			'type':SEND_CLOSE_LOBBY_TYPE,
			'lobby_id':self.lobby_id,
			# 'user_id':self.user_id
		}
		for sock in list(online_users.values()):
			sock.write_message(json.dumps(message))

	def open(self, lobby_id, user_id):
		self.user_id = user_id
		self.lobby_id = lobby_id
		online_users[user_id] = self

		# client = httpclient.AsyncHTTPClient()
		# request = httpclient.HTTPRequest(
		# 	'http://127.0.0.1:8000/api/lobbies/%s/' % self.lobby_id,
		# 	method='PATCH',
		# 	body=urllib.parse.urlencode({
		# 		'players':[self.user_id,],
		# 	})
		# )
		# client.fetch(request, self.handle_request)

	def on_message(self, message):
		try:
			message_json = json.loads(message, object_pairs_hook=OrderedDict)
		except Exception as err:
			self.write_message('Wrong json format')
			return

		if 'type' in message_json:
			if message_json['type'] == SEND_HINT_MESSAGE_TYPE:
				# if 'user_id' in message_json:
				self.send_hint_message()
				# else:
				# 	self.write_message(
				# 		'Wrong %s method format.' % SEND_HINT_MESSAGE_TYPE
				# 	)
			elif message_json['type'] == SEND_FIGURE_MESSAGE_TYPE:
				if 'figure' in message_json:
					self.send_figure_message(message_json['figure'])
				else:
					self.write_message(
						'Wrong %s method format.' % SEND_FIGURE_MESSAGE_TYPE
					)
			elif message_json['type'] == SEND_START_GAME_TYPE:
				self.send_start_game()
			elif message_json['type'] == SEND_USER_ENTER_TYPE:
				if 'username' in message_json:
					self.send_user_enter(message_json['username'])
				else:
					self.write_message(
						'Wrong %s method format.' % SEND_USER_ENTER_TYPE
					)
			else:
				self.write_message('Method Not Allowed.')
		else:
			self.write_message('Argument "type" is absent.')

	def on_close(self):
		sockets = list(online_users.values())
		if self in sockets:
			idx = sockets.index(self)
			id_list = list(online_users.keys())
			del(online_users[id_list[idx]])
		self.send_close_lobby()
		print('Lobby socket closed')
