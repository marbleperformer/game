import signal
import time

from tornado import web
from tornado import ioloop
from tornado import httpserver

from django.core.management.base import BaseCommand

from app.handlers import MainMessageHandler

class Command(BaseCommand):

    def sig_handler(self, sig, frame):
        ioloop.IOLoop.instance().add_callback(self.shutdown)

    def shutdown(self):
        self.http_server.stop()

        io_loop = ioloop.IOLoop.instance()
        io_loop.add_timeout(time.time() + 2, io_loop.stop)

    def handle(self, *args, **options):

        app = web.Application(
            [
                (r'/users/(?P<user_id>\d+)/', MainMessageHandler),
            ]
        )

        self.http_server = httpserver.HTTPServer(app)
        self.http_server.listen(8888)

        # Init signals handler
        signal.signal(signal.SIGTERM, self.sig_handler)

        # This will also catch KeyboardInterrupt exception
        signal.signal(signal.SIGINT, self.sig_handler)

        ioloop.IOLoop.instance().start()