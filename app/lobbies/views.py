from django.shortcuts import render, redirect

from django.core.urlresolvers import reverse, reverse_lazy

from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin

from django.views.generic import (
	ListView, DetailView, CreateView, UpdateView, DeleteView
)

from rest_framework import viewsets

from rest_framework import filters

from rest_framework import mixins

from .serializers import LobbySerializer, HintSerializer, TurnSerializer

from mixins import LobbyOpenedRequiredMixin, LobbyWatingRequiredMixin

from .models import Lobby, Hint, Turn

class LobbyListView(LoginRequiredMixin, ListView):
	template_name = 'lobbies/index.html'
	login_url = 'login'

	queryset = Lobby.objects.all()

	context_object_name = 'collection'

	def get_queryset(self):
		return self.queryset.filter(closed=None)

	def get_context_data(self, **kwargs):
		context = super(LobbyListView, self).get_context_data(**kwargs)
		context['users'] = User.objects.all()
		return context

class PlaygroundView(LoginRequiredMixin, 
					 LobbyOpenedRequiredMixin,
					 LobbyWatingRequiredMixin,
					 DetailView):
	template_name = 'lobbies/playground.html'
	lobby_access_denied_url = 'lobbies:index'
	login_url = 'login'

	model = Lobby

class LobbyDetailView(LoginRequiredMixin, DetailView):
	template_name = 'lobbies/detail.html'
	login_url = 'login'

	model = Lobby

class LobbyCreateView(LoginRequiredMixin, CreateView):
	template_name = 'lobbies/edit.html'

	success_url = 'lobbies:playground'
	login_url = 'login'

	model = Lobby

	fields = ['name', 'timer']

	def get_success_url(self, *args, **kwargs):
		return reverse(self.success_url, kwargs={'pk': self.object.pk})

	def form_valid(self, form):
		form.instance.owner = self.request.user
		return super(LobbyCreateView, self).form_valid(form)

class LobbyUpdateView(LoginRequiredMixin, UpdateView):
	template_name = 'lobbies/edit.html'

	success_url = 'lobbies:index'
	login_url = 'login'

	model = Lobby

	fields = ['name', 'timer']

class LobbyDeleteView(LoginRequiredMixin, DeleteView):
	template_name = 'lobbies/delete.html'

	success_url = 'lobbies:index'
	login_url = 'login'

	model = Lobby

class LobbyViewSet(viewsets.ModelViewSet):
	filter_backends = (filters.DjangoFilterBackend,)
	filter_fields = ('id', 'owner', 'status')

	queryset = Lobby.objects.all()
	serializer_class = LobbySerializer

	# owner auto set
	def perform_create(self, serializer):
		serializer.save(owner=self.request.user)

class HintViewSet(viewsets.ModelViewSet):
	queryset = Hint.objects.all()
	serializer_class = HintSerializer

	# player auto set
	def perform_create(self, serializer):
		serializer.save(player=self.request.user)

class TurnViewSet(viewsets.ModelViewSet):
	queryset = Turn.objects.all()
	serializer_class = TurnSerializer

	# player auto set
	def perform_create(self, serializer):
		serializer.save(player=self.request.user)
