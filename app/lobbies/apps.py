from django.apps import AppConfig


class LobbiesConfig(AppConfig):
    name = 'lobbies'
