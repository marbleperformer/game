function EnvironmentUIManager(environment, settings) {
		this.env_selector = '[environment="$"]'.replace('$', environment)
		this.table_selector = this.env_selector + ' table'
		this.empty_selector = this.env_selector + ' .collection-empty'
		this.item_selector = this.env_selector + ' [item-id]'

		this.settings = settings

		function get_env_selector(element) {
			env = null
			while (!env) {
				env = element.parent().attr('environment')
				element = element.parent()
			}
			selector = '[environment="' + env + '"]' 
			return selector
		}

		set_controls_enable = function(event) {
			event.stopPropagation()
			row = event.target.parentNode
			id = row.getAttribute('item-id')
			is_clickable = row.classList.contains('clickable')

			if(!id) {return}

			control_selector = get_env_selector($(this)) + ' .control'

			$(control_selector).each((index, element) => {
				pattern = $(element).attr('pattern')
				if (pattern) {
					if (is_clickable) {
						$(element).removeClass('disabled')
						url = pattern.replace('$', id)
						$(element).attr('href', url)
					} else {
						$(element).addClass('disabled')
					}
				}
			})
		}

		set_row_selection = function(event) {
			event.stopPropagation()
			row = event.target.parentNode
			id = row.getAttribute('item-id')
			is_clickable = row.classList.contains('clickable')

			if(!id) {return}

			selected_selector = get_env_selector($(this)) + ' .info'

			$(selected_selector).removeClass('info')

			if (is_clickable) {
				$(row).addClass('info')
			}
		}

		this.add = function(data) {
			row_string = '<tr item-id="$"></tr>'.replace('$', data.id)
			row_selector = this.env_selector + ' tr[item-id="$"]'.replace('$', data.id)
			if ($(row_selector).length == 0) {
				$(this.table_selector).append(row_string)
				for (var idx = 0; idx < this.settings.fields.length; idx++) {
					field_name = this.settings.fields[idx]
					col_string = '<td class="$c">$v</td>'.replace('$c', (field_name + '-col'))
					col_string = col_string.replace('$v', data[field_name])
					$(row_selector).append(col_string)
				}
			} else {
				console.log('item with id ' + data.id + ' is already exist')
			}

			$(row_selector).on('click', set_row_selection)
			$(row_selector).on('click', set_controls_enable)

			if ($(this.empty_selector).length != 0) {
				$(this.empty_selector).remove()
			}
		}

		this.update = function(data) {
			row_selector = this.env_selector + ' tr[item-id="$"]'.replace('$', data.id)
			col_selector_pattern = row_selector + ' .$-col'
			for (var idx = 0; idx < this.settings.fields.length; idx++) {
				field_name = this.settings.fields[idx]
				col_selector = col_selector_pattern.replace('$', field_name)
				if (data[field_name]) {
					$(col_selector).text(data[field_name])
				}
			}
		}

		this.delete = function(data) {
			row_selector = this.env_selector + ' tr[item-id="$"]'.replace('$', data.id)
			$(row_selector).remove()

			if ($(this.item_selector).length == 0) {
				if ($(this.empty_selector).length == 0) {
					empty_string = '<tr class="collection-empty"><td colspan="$cs">$t</td></tr>'.replace('$t', settings.empty_text)
					empty_string = empty_string.replace('$cs', this.settings.fields.length)
					$(this.table_selector).append(empty_string)
				}
			}
		}

		$(this.item_selector).each((index, element) => {
			$(element).on('click', set_controls_enable)
			$(element).on('click', set_row_selection)
		})
	}

function remove_all_selections (event) {
	$('[environment] .info').each((index, element) => {
		$(element).removeClass('info')
	})
	$('[environment] [pattern]').each((index, element) => {
		$(element).addClass('disabled')
	})
}

window.addEventListener('click', remove_all_selections)