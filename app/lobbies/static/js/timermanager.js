function TimerManager(element) {
	this.element = element

	this.funcs = []

	this.subscribe = function(func) {
		this.funcs.push(func)
	}

	this.start = function(time) {
		this.interval = setInterval(() => {$(this.element).text((time--))}, 1000)
		this.timeout = setTimeout(() => {
				clearTimeout(this.timeout)
				clearInterval(this.interval)
				this.funcs.forEach(func => {func.call(null)})
		},1000 * time + 1000);
	}

	this.stop = function() {
		clearTimeout(this.timeout)
		clearInterval(this.interval)
	}
}
