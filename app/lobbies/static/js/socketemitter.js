function SocketEmitter(channel, sock) {
	this.events = {}

	this.sock = sock
	this.channel = channel
	this.identity = null

	this.sock.addEventListener('open', this.on_open)
	this.sock.addEventListener('close', this.on_close)
	this.sock.addEventListener('message', event => {
		message = JSON.parse(event.data)
		if (this.channel == message.channel){
			if (message.method == 'INIT') {
				if (!this.identity) {
					this.identity = message.identity
				}
				if (message.identity == this.identity) {
					this.emitt('init', {body:message.body, identity:message.identity})
				}
			}
			else if (message.method == 'OPEN') {
				this.emitt('open', {body:message.body, identity:message.identity})
			} else if (message.method == 'CLOSE') {
				this.emitt('close', {body:message.body, identity:message.identity})
			} else if (message.method == 'POST') {
				this.emitt('post', {body:message.body, identity:message.identity})
			} else if (message.method == 'DELETE') {
				this.emitt('delete', {body:message.body, identity:message.identity})
			} else if (message.method == 'PATCH') {
				this.emitt('patch', {body:message.body, identity:message.identity})
			}
		}
	})

	function send_open_message(event) {
		sock.send(JSON.stringify({'channel':channel, 'method':'OPEN'}))
	}

	this.sock.addEventListener('open', send_open_message)

	this.emitt =function(event_name, data) {
		const event = this.events[event_name]
		if (event) {
			event.forEach(func => {
				func.call(null, data)				
			})
		}
	}

	this.subscribe = function(event_name, func) {
		if(!this.events[event_name]) {
			this.events[event_name] = []
		}
		this.events[event_name].push(func)
	}

	this.post = function(data) {
		message = {
			'method':'POST',
			'channel':this.channel,
			'body':data
		}
		this.sock.send(JSON.stringify(message))
	}

	this.patch = function(data) {
		message = {
			'method':'PATCH',
			'channel':this.channel,
			'body':data
		}
		this.sock.send(JSON.stringify(message))
	}

	this.delete = function(data) {
		message = {
			'method':'DELETE',
			'channel':this.channel,
			'body':data
		}
		this.sock.send(JSON.stringify(message))
	}

	this.close = function() {
		this.sock.close()
	}
}
