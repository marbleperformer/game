function set_links(event) {
	event.stopPropagation()

	// row properties
	row = event.target.parentNode
	id = row.getAttribute('item-id')

	// return if row is not item
	if(!id) {
		return
	}

	// row properties
	is_clickable = row.classList.contains('clickable')
	environment = row.getAttribute('environment')
	active_pattern = '[environment="$"].info'

	// control properties
	control_pattern = '[environment="$"].control'
	control_selector = control_pattern.replace('$', environment)

	controls = document.querySelectorAll(control_selector)

	// set controls urls or disable
	for (idx = 0; idx < controls.length; idx++) {
		control = controls[idx]
		pattern = control.getAttribute('pattern')
		if (pattern) {
			if (is_clickable) {
				control.classList.remove('disabled')
				url = pattern.replace('$', id)
				control.href = url
			} else {
				control.classList.add('disabled')
			}
		}
	}

	// remove active row selection
	active_selector = active_pattern.replace('$', environment)
	active = document.querySelector(active_selector)
	if (active) {
		active.classList.remove('info')
	}

	console.log(is_clickable)

	// set current row selection
	if (is_clickable) {
		row.classList.add('info')
	}
}

function remove_all_selections() {
	items = document.querySelectorAll('[environment].info')
	for (var idx = 0; idx < items.length; idx++) {
		item = items[idx]
		item.classList.remove('info')
	}

	controls = document.querySelectorAll('[environment][pattern]')
	for (var idx = 0; idx < controls.length; idx++) {
		control = controls[idx]
		control.classList.add('disabled')
	}

}

rows = document.querySelectorAll('table.table tbody tr')
for (idx=0; idx<rows.length; idx++) {
	row = rows[idx]
	row.addEventListener('click', set_links)
}

window.addEventListener('click', remove_all_selections)