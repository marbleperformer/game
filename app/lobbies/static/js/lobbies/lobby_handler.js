function add_new_lobby(id) {
	lobby_api_pattern = '/api/lobbies/$'
	request = new XMLHttpRequest()
	lobby_api_url = lobby_api_pattern.replace('$', id)
	table_body = document.querySelector('table[id="lobbies"] tbody')
	request.open('GET', lobby_api_url, true)
	request.onreadystatechange = function() {
		if (request.readyState == 4 && request.status == 200) {
			lobby = JSON.parse(request.responseText)

			row = document.createElement('tr')
			row.setAttribute('item-id', lobby.id)
			row.setAttribute('environment', 'lobbies')
			row.setAttribute('item-status', 'WTN')
			row.classList.add('clickable')
			row.classList.add('success')

			name_col = document.createElement('td')
			name_col.innerHTML = lobby.name

			timer_col = document.createElement('td')
			timer_col.innerHTML = lobby.timer

			owner_col = document.createElement('td')
			owner_col.innerHTML = lobby.owner

			status_col = document.createElement('td')
			status_col.classList.add('status-col')
			status_col.innerHTML = 'Waiting for players'

			row.appendChild(name_col)
			row.appendChild(timer_col)
			row.appendChild(owner_col)
			row.appendChild(status_col)

			table_body.appendChild(row)

			row.addEventListener('click', set_links)
		}
	}
	request.send()
}

function set_lobby_wating(id) {
	lobby_pattern = '[environment="lobbies"][item-id="$"]'
	lobby_selector = lobby_pattern.replace('$', id)
	// console.log(lobby_selector)
	lobby_item = document.querySelector(lobby_selector)
	// console.log(lobby_item)
	if (!lobby_item) {
		add_new_lobby(id)
		return
	}
	lobby_item.classList.remove('warning')
	lobby_item.classList.remove('danger')
	lobby_item.classList.add('success')
	status_col = lobby_item.querySelector('.status-col')
	status_col.innerHTML = 'Waiting for players'
}

function set_lobby_ready(id) {
	lobby_pattern = '[environment="lobbies"][item-id="$"]'
	lobby_selector = lobby_pattern.replace('$', id)

	// console.log(lobby_selector)

	lobby_item = document.querySelector(lobby_selector)
	if (!lobby_item) {
		return
	}
	lobby_item.classList.remove('clickable')
	lobby_item.classList.remove('success')
	lobby_item.classList.remove('danger')
	lobby_item.classList.add('warning')
	status_col = lobby_item.querySelector('.status-col')
	status_col.innerHTML = 'Ready to start'
}

function set_lobby_inprocess(id) {
	lobby_pattern = '[environment="lobbies"][item-id="$"]'
	lobby_selector = lobby_pattern.replace('$', id)
	lobby_item = document.querySelector(lobby_selector)
	if (!lobby_item) {
		return
	}
	lobby_item.classList.remove('warning')
	lobby_item.classList.remove('warning')
	lobby_item.classList.add('danger')
	status_col = lobby_item.querySelector('.status-col')
	status_col.innerHTML = 'Game in process'
}

function set_lobby_closed(id) {
	lobby_pattern = '[environment="lobbies"][item-id="$"]'
	lobby_selector = lobby_pattern.replace('$', id)
	lobby_item = document.querySelector(lobby_selector)
	if (!lobby_item) {
		return
	}
	lobby_item.parentNode.removeChild(lobby_item)
}

function set_lobby_status(event){
	message = JSON.parse(event.data)
	console.log(message)
	if (message.type = 'SLS') {
		if (message.status == 'WTN') {
			set_lobby_wating(message.lobby_id)
		} else if (message.status == 'RED') {
			set_lobby_ready(message.lobby_id)
		} else if (message.status == 'PRC') {
			set_lobby_inprocess(message.lobby_id)
		} else if (message.status == 'CLS') {
			set_lobby_closed(message.lobby_id)
		}
	} 
	// else if (message.type == 'SCL') {
	// 	set_lobby_closed(message.lobby_id)
	// }
}

user_ws.addEventListener('message', set_lobby_status)