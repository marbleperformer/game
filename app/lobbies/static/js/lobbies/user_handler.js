function get_online_users(event) {
		message = {"type":"GOU"}
		user_ws.send(JSON.stringify(message))
	}

function add_new_user(id) {
	user_api_pattern = '/api/users/$'
	request = new XMLHttpRequest()
	user_api_url = user_api_pattern.replace('$', id)
	table_body = document.querySelector('table[id="users"] tbody')
	request.open('GET', user_api_url, true)
	request.onreadystatechange = function() {
		if (request.readyState == 4 && request.status == 200) {
			user = JSON.parse(request.responseText)

			row = document.createElement('tr')
			row.setAttribute('item-id', user.id)
			row.setAttribute('environment', 'users')
			row.classList.add('clickable')
			row.classList.add('success')

			name_col = document.createElement('td')
			name_col.innerHTML = user.username

			status_col = document.createElement('td')
			status_col.classList.add('status-col')
			status_col.innerHTML = 'Onlie'

			row.appendChild(name_col)
			row.appendChild(status_col)

			table_body.appendChild(row)
		}
	}
	request.send()
}

function set_user_online(id) {
	user_pattern = '[environment="users"][item-id="$"]'
	user_selector = user_pattern.replace('$', id)
	user_item = document.querySelector(user_selector)
	if (!user_item) {
		add_new_user(id)
		return
	}
	user_item.classList.add('success')
	status_col = user_item.querySelector('.status-col')
	status_col.innerHTML = 'Onlie'
}

function set_user_offline(id) {
	user_pattern = '[environment="users"][item-id="$"]'
	user_selector = user_pattern.replace('$', id)
	user_item = document.querySelector(user_selector)
	user_item.classList.remove('success')
	status_col = user_item.querySelector('.status-col')
	status_col.innerHTML = 'Offline'
}

function set_online_users_status(event) {
	message = JSON.parse(event.data)
	if (message.type == 'GOU') {
		for (var idx = 0; idx < message.users.length; idx++) {
		user_id = message.users[idx]
			set_user_online(user_id)
		}
	}
}

function set_user_status(event) {
	message = JSON.parse(event.data)
	if (message.type == 'SUS') {
		if (message.status == 'online') {
			set_user_online(message.user_id)
		} else {
			set_user_offline(message.user_id)
		}
	}
}

user_ws.addEventListener('open', get_online_users)
user_ws.addEventListener('message', set_online_users_status)
user_ws.addEventListener('message', set_user_status)