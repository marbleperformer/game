// timer is a global playground.html module variable
// will be executed when timer time is up
timer.subscribe(() => {
	// tour is a global playground.html variable
	tour++

	// figure is a global playground.html variable
	figure = null

	turns_api_url = '/api/turns/'

	// lobby_id is a global playground.html constant 
	// figure is a global playground.html variable
	turn_data = {type:'turn', tour:tour, figure:figure, lobby:lobby_id}

	// current_lobby_emitter is a global playground.html module variable
	current_lobby_emitter.post(turn_data)

	$.ajax({
		url:turns_api_url,
		type:'POST',
		data: turn_data,
		headers: {'X-CSRFToken': getCookie('csrftoken')},
		dataType:'application/json',
	})
})

// current_lobby_emitter is a global playground.html module variable
// will be executed when opponent will call current_lobby_emitter patch 
current_lobby_emitter.subscribe('patch', message => {
	// lobby_owner is a global playground.html constant 
	// if current user is lobby owner
	if (current_lobby_emitter.identity == lobby_owner) {
		// if user is trying to connect to game
		if (message.body.status == 'RED') {
			// opponent_id is a global playground.html variable
			opponent_id = message.identity
			$('#start-modal').modal('show')
		} 
	}
	// if owner is declined to game with current user
	else if (message.body.status == 'WTN') {
		current_lobby_emitter.close()
		$('#stop-modal').modal('show')
	}

	// if owner is declined to game with current user
	if (message.body.status == 'WTN') {
		$('#player-panel').text('')

		$('#stop-panel').text('Your game was declined')
	} 
	// if user is trying to connect to game
	else if (message.body.status == 'RED') { 
		user_api_url = '/api/users/' + message.identity + '/'
		$.get(user_api_url, data => {
			$('#player-panel').text(data.username)

			$('#cabdidate-panel').text(data.username)
		})
	} 
	// if owner is accept game with current user
	else if (message.body.status == 'PRC') {
		// timer is a global playground.html module variable
		// lobby_timer is a global playground.html module constant
		timer.start(lobby_timer)
			
		$('.control').each((index, element) => {
			$(element).removeClass('disabled')				
		})
	}
})

// current_lobby_emitter is a global playground.html module variable 
// will be executed when opponent socket will be closed 
current_lobby_emitter.subscribe('close', message => {
	// timer is a global playground.html module variable
	timer.stop()
	
	// lobby_api_url is a global playground.html constant
	// get an actual data of current lobby
	$.get(lobby_api_url, data => {
		now = new Date()
		// lobby_id is a global playground.html constant 
		// if current user is lobby owner
		if (lobby_owner == current_lobby_emitter.identity) {
			// if candidate socket was closed before game was started
			if (data.status == 'RED' || data.status == 'WTN'){
				data.status = 'WTN'
				data.players = data.owner

				// lobbies_emitter is a global playground.html module variable
				lobbies_emitter.patch(data)
				$('#start-modal').modal('hide')
			}
		} else {
			// if game is was not started at the moment
			if (data.status == 'RED') {
				data.closed = now.toISOString()
				lobbies_emitter.delete(data)

				$('#stop-panel').text("Unfortunately, you can't start game in this lobby because it was closed")
				$('#stop-modal').modal('show')
			}
		}
		// if game is was started at the moment
		if (data.status == 'PRC') {
			// game_ended is a global playground.html variable 
			// if game hasn't set winner and game_ended variable id false 
			if (!data.winner && !game_ended) {
				data.winner = current_lobby_emitter.identity
				data.closed = now.toISOString()
				lobbies_emitter.delete(data)
				$('#stop-panel').text('Your opponent is leaving the game, you win!')
				$('#stop-modal').modal('show')
			}
		}

		$.ajax({
			url:lobby_api_url,
			type:'PATCH',
			data: data,
			headers: {'X-CSRFToken': getCookie('csrftoken')},
			dataType:'application/json',
		})
	})
})
