// lobbies_emitter is a global playground.html module variable
// will be executed when current user socket will be open 
lobbies_emitter.subscribe('init', message => {
	// lobby_api_url is a global playground.html constant
	$.get(lobby_api_url, data => {
		// lobby_owner is a global playground.html constant
		// if message sender is lobby owner
		if (lobby_owner == message.identity) {
			data['status'] = 'WTN'
			lobbies_emitter.post(data)
		} else {
			data['status'] = 'RED'
			data['players'] = message.identity
			// current_lobby_emitter is a global playground.html module variable
			current_lobby_emitter.patch(data)
			lobbies_emitter.patch(data)
		}
		$.ajax({
			url:lobby_api_url,
			type:'PATCH',
			data: data,
			headers: {'X-CSRFToken': getCookie('csrftoken')},
			dataType:'application/json'
		})
	})
})
