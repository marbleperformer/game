// playground variables
wins = 0

// playground constants
const figure_list = ['ROC', 'PAP', 'SCI', 'LIZ', 'SPC']

const rules = {
	ROC:['PAP', 'SPC'],
	PAP:['SCI', 'LIZ'],
	SCI:['ROC', 'SPC'],
	LIZ:['ROC', 'SCI'],
	SPC:['PAP', 'LIZ'],
	null:figure_list
}

// playground handler methods
function handle_turn(message) {
	// if current user has made him turn at the moment
	if (message.body.tour == tour) {
		// figure is a global playground.html variable
		data = {type:'turn_result', tour:tour, figure:figure}
	} else {
		data = {type:'wait'}
	}

	turn_id = 'u' + message.identity +  't' + message.body.tour

	turn_string = '<div id="$id" class="hidden" style="text-align:$fl; display:block; width:100%;">\
		<div style="width: 150px; height: 150px; display:inline-block;">\
			<img style="width:100; height: 100%" src="/static/media/icons/$val.svg"/>\
		</div>\
	</div>'.replace('$id', turn_id)
	turn_string = turn_string.replace('$val', message.body.figure)
	// if message sender is user owner
	if (message.identity == lobby_owner) {
		turn_string = turn_string.replace('$fl', 'left')
	} else {
		turn_string = turn_string.replace('$fl', 'right')
	}

	$('#playground').append(turn_string)

	// current_lobby_emitter is a global playground.html module variable
	current_lobby_emitter.post(data)
}

function handle_turn_result(message) {
	data = {type:'continue', timer:lobby_timer, winner:null}
	// figure is a global playground.html variable
	// if opponent turn figure is not the same the current user figure
	if (message.body.figure != figure) {
		// if rules list of user figure is not contains opponent figure
		if (rules[figure].indexOf(message.body.figure) == -1) {
			// current_lobby_emitter is a global playground.html module variable
			data['winner'] = current_lobby_emitter.identity
		} else {
			data['winner'] = message.identity
		}
	}
	current_lobby_emitter.post(data)
}

function handle_hint(message) {
	data = {type:'hint_result', figures:[]}
	// tour is a global playground.html variable
	// if user made him turn at the moment
	if (message.body.tour != tour) {
		// figure is a global playground.html variable
		rand_figure = figure
		while (rand_figure == figure) {
			rand_idx = Math.floor(Math.random() * figure_list.length) 
			rand_figure = figure_list[rand_idx]
		}
		figure_list.forEach(fig => {
			// if figure is not the same random figure and not the same current turn figure
			if (fig != figure && fig != rand_figure) {
				data.figures.push(fig)
			}
		})
	}

	turn_id = 'u' + message.identity +  't' + message.body.tour

	
	turn_string = '<div id="$id" style="text-align:$fl; display:block; width:100%;">\
		<div style="width: 150px; height: 150px; display:inline-block;">\
			<img style="width:100; height: 100%" src="/static/media/icons/HINT.svg"/>\
		</div>\
	</div>'.replace('$id', turn_id)
	// if message sender is lobby owner
	if (message.identity == lobby_owner) {
		turn_string = turn_string.replace('$fl', 'left')
	} else {
		turn_string = turn_string.replace('$fl', 'right')
	}

	$('#playground').append(turn_string)

	// current_lobby_emitter is a global playground.html module variable
	current_lobby_emitter.post(data)
}

function handle_hint_result(message) {
	message.body.figures.forEach(id => {
		selector = '#' + id
		$(selector).addClass('disabled')
	})
}

function handle_continue(message) {
	$('.control.figure').each((index, element) => {
		$(element).removeClass('disabled')				
	})


	turn_id = '#u' + opponent_id +  't' + tour
	console.log(turn_id)
	console.log($(turn_id))
	$(turn_id).removeClass('hidden')

	// hint_used is a global playground.html variable
	if (!hint_used) {
		$('#HINT').removeClass('disabled')
	}

	if (message.body.winner) {
		// current_lobby_emitter is a global playground.html module variable
		if (message.body.winner == current_lobby_emitter.identity){
			wins++
		}
	}

	// wins is a global playground.html variable
	if (wins >= 3) {
		current_lobby_emitter.post({type:'end'})
		return
	}

	// timer is a global playground.html module variable
	timer.start(message.body.timer)
}

function handle_end(message) {
	// timer is a global playground.html module variable
	timer.stop()

	now = new Date()

	// game_ended is a global playground.html variable
	game_ended = true

	// current_lobby_emitter is a global playground.html module variable
	if (current_lobby_emitter.identity != message.identity) {
		// lobby_api_url is a global playground.html constant
		$.get(lobby_api_url, data => {
			// lobbies_emitter is a global playground.html module variable
			lobbies_emitter.delete(data)
			data['closed'] = now.toISOString()
			data['winner'] = message.identity

			$.ajax({
				url:lobby_api_url,
				type:'PATCH',
				data: data,
				headers: {'X-CSRFToken': getCookie('csrftoken')},
				dataType:'application/json',
				complete: data => {
					console.log(data)
				}
			})
		})

		$('#stop-panel').text('Unfortunately, you lose')

	} else {
		$('#stop-panel').text('Congratulations, you win!')
	}

	$('#stop-modal').modal('show')
}

// current_lobby_emitter is a global playground.html module variable
current_lobby_emitter.subscribe('post', message => {
	// if message sender is not current user
	if (current_lobby_emitter.identity != message.identity){
		if (message.body.type == 'turn') {
			handle_turn(message)
		} else if (message.body.type == 'wait') {
			console.log('Wait')
		} else if (message.body.type == 'turn_result') {
			handle_turn_result(message)
		} else if (message.body.type == 'hint') {
			handle_hint(message)
		} else if (message.body.type == 'hint_result') {
			handle_hint_result(message)			
		}
	}
	if (message.body.type == 'continue') {
		handle_continue(message)
	} else if (message.body.type == 'end') {
		handle_end(message)
	}

	$('#playground').scrollTop($('#playground').prop('scrollHeight'))
})