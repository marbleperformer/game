$('#start-btn').on('click', event => {
	$('#start-modal').modal('hide')
	// lobby_api_url is a global playground.html constant
	$.get(lobby_api_url, data => {
		data.status = 'PRC'
		// lobbies_emitter is a global playground.html module variable
		lobbies_emitter.patch(data)
		// current_lobby_emitter is a global playground.html module variable 
		current_lobby_emitter.patch(data)

		$.ajax({
			url:lobby_api_url,
			type:'PATCH',
			data: data,
			headers: {'X-CSRFToken': getCookie('csrftoken')},
			dataType:'application/json',
		})
	})
})

$('#decline-btn').on('click', event => {
	$('#start-modal').modal('hide')
	data = {status: 'WTN'}
	// current_lobby_emitter is a global playground.html module variable 
	current_lobby_emitter.patch(data)
})

$('.figure').each((index, element) => {
	turns_api_url = '/api/turns/'
	$(element).on('click', () => {
		// tour is a global playground.html variable
		tour++
		// figure is a global playground.html variable
		figure = $(element).attr('id')

		$('.control').each((index, element) => {
			$(element).addClass('disabled')
		})

		turn_data = {type:'turn', tour:tour, figure:figure, lobby:lobby_id}
		// current_lobby_emitter is a global playground.html module variable 
		current_lobby_emitter.post(turn_data)
		// timer is a global playground.html module variable
		timer.stop()

		turn_id = 'u' + current_lobby_emitter.identity +  't' + tour

		turn_string = '<div id="$id" style="text-align:$fl; display:block; width:100%;">\
			<div style="width: 150px; height: 150px; display:inline-block;">\
				<img style="width:100; height: 100%" src="/static/media/icons/$val.svg"/>\
			</div>\
		</div>'.replace('$id', turn_id)
		turn_string = turn_string.replace('$val', figure)
		// if current user is not lobby owner
		if (current_lobby_emitter.identity == lobby_owner) {
			turn_string = turn_string.replace('$fl', 'left')
		} else {
			turn_string = turn_string.replace('$fl', 'right')
		}

		$('#playground').append(turn_string)

		$.ajax({
			url:turns_api_url,
			type:'POST',
			data: turn_data,
			headers: {'X-CSRFToken': getCookie('csrftoken')},
			dataType:'application/json'
		})
	})
})

$('#HINT').on('click', () => {
	// hint_used is a global playground.html variable
	hint_used = true
	hints_api_url = '/api/hints/'
	$('#HINT').addClass('disabled')

	hint_data = {type:'hint', tour:tour, lobby:lobby_id}

	// current_lobby_emitter is a global playground.html module variable
	current_lobby_emitter.post(hint_data)

	turn_id = 'u' + current_lobby_emitter.identity +  't' + tour + 'hint'

	turn_string = '<div id="$id" style="text-align:$fl; display:block; width:100%;">\
		<div style="width: 150px; height: 150px; display:inline-block;">\
			<img style="width:100; height: 100%" src="/static/media/icons/HINT.svg"/>\
		</div>\
	</div>'.replace('$id', turn_id)
	turn_string = turn_string.replace('$val', figure)
	
	// if current user is lobby owner
	if (current_lobby_emitter.identity == lobby_owner) {
		turn_string = turn_string.replace('$fl', 'left')
	} else {
		turn_string = turn_string.replace('$fl', 'right')
	}

	$('#playground').append(turn_string)

	$.ajax({
		url:hints_api_url,
		type:'POST',
		data: hint_data,
		headers: {'X-CSRFToken': getCookie('csrftoken')},
		dataType:'application/json',
	})
})
