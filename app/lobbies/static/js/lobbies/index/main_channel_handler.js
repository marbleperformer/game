users_emitter = new SocketEmitter(null, main_socket)

users_emitter.subscribe('init', message => {
	selector_pattern = '[environment="users"] [item-id="$"]'
	message.body.online_users.forEach(user_id => {
		selector = selector_pattern.replace('$', user_id)
		users_manager.update({'id':user_id, 'status':'Online'})
		$(selector).addClass('success')
	})
})

users_emitter.subscribe('open', message => {
	// console.log(message)
	selector_pattern = '[environment="users"] [item-id="$"]'
	selector = selector_pattern.replace('$', message.body.identity)
	if ($(selector).length == 0) {
		api_url = '/api/users/$'.replace('$', message.body.identity)
		$.get(api_url, (data) => {
			data['status'] = 'Online'
			users_manager.add(data)
			$(selector).addClass('success')
		})
	} else {
		users_manager.update({'id':message.body.identity, 'status':'Online'})
		$(selector).addClass('success')
	}
	
})

users_emitter.subscribe('close', message => {
	// console.log(message)

	selector_pattern = '[environment="users"] [item-id="$"]'
	selector = selector_pattern.replace('$', message.identity)
	users_manager.update({'id':message.body.identity, 'status':'Offline'})
	$(selector).removeClass('success')
})