lobbies_emitter = new SocketEmitter('lobbies', main_socket)

lobbies_emitter.subscribe('post', message => {
	selector_pattern = '[environment="lobbies"] [item-id="$"]'
	api_url = '/api/users/' + message.body.owner + '/'

	$.get(api_url, data => {
		message.body['owner'] = data.username
		
		selector = selector_pattern.replace('$', message.body.id)

		message.body.status = 'Waiting for players'

		lobbies_manager.add(message.body)
		$(selector).addClass('success clickable')
	})
	
})


lobbies_emitter.subscribe('patch', message => {
	selector_pattern = '[environment="lobbies"] [item-id="$"]'
	selector = selector_pattern.replace('$', message.body.id)

	if (message.body.status == 'WTN') {
		message.body.status = 'Waiting for players'
		$(selector).removeClass('warning danger')
		$(selector).addClass('success clickable')
	} else if (message.body.status == 'RED') {
		message.body.status = 'Ready to start'
		$(selector).removeClass('success danger clickable')
		$(selector).addClass('warning')
	} else if (message.body.status == 'PRC') {
		message.body.status = 'Game in process'
		$(selector).removeClass('success warning clickable')
		$(selector).addClass('danger')
	}

	lobbies_manager.update(message.body)
})

lobbies_emitter.subscribe('delete', message => {
	console.log(message)
	selector_pattern = '[environment="lobbies"] [item-id="$"]'
	selector = selector_pattern.replace('$', message.body.id)

	lobbies_manager.delete(message.body)
})

lobbies_emitter.subscribe('close', message => {
	now = new Date()
	api_url = '/api/lobbies/?status=WTN&owner=' + message.identity
	$.get(api_url, data=> {
		data.forEach(lobby => {
			if (!lobby.closed) {
				api_url = '/api/lobbies/' + lobby.id + '/'
				lobby.closed = now.toISOString()
				$.ajax({
					url:api_url,
					type:'PATCH',
					data: lobby,
					headers: {'X-CSRFToken': getCookie('csrftoken')},
					dataType:'application/json'
				})
				lobbies_emitter.delete(lobby)
				// }
			}
		})
	})
})

// lobbies_emitter.subscribe('init', message => {

// 	if (message.identity == lobbies_emitter.identity) {
// 		now = new Date()
// 		api_url = '/api/lobbies/?status=WTN&owner=' + message.identity
// 		$.get(api_url, data=> {
// 			data.forEach(lobby => {
// 				if (!lobby.closed) {
// 					api_url = '/api/lobbies/' + data[idx].id + '/'
// 					lobby.closed = now.toISOString()
// 					$.ajax({
// 						url:api_url,
// 						type:'PATCH',
// 						data: lobby,
// 						headers: {'X-CSRFToken': getCookie('csrftoken')},
// 						dataType:'application/json'
// 					})
// 					lobbies_emitter.delete(lobby)
// 				}
// 			})
// 		})
// 	}
// })