lobbies_emitter = new SocketEmitter('lobbies', main_socket)

lobbies_emitter.subscribe('post', message => {
	selector_pattern = '[environment="lobbies"] [item-id="$"]'
	console.log(selector_pattern)
	
	lobbies_manager.add(message.body)

	selector = selector_pattern.replace('$', message.body.id)


	if (message.body.status == 'WTN') {
		message.body.status = 'Waiting for players'
		// $(selector).removeClass('warning danger')
		$(selector).addClass('success clickable')
	}

	// console.log()

	// $(selector).addClass('success clickable')
})

lobbies_emitter.subscribe('patch', message => {
	// console.log(message)
	selector_pattern = '[environment="lobbies"] [item-id="$"]'
	selector = selector_pattern.replace('$', message.body.id)

	if (message.body.status == 'WTN') {
		message.body.status = 'Waiting for players'
		$(selector).removeClass('warning danger')
		$(selector).addClass('success clickable')
	} else if (message.body.status == 'RED') {
		message.body.status = 'Ready to start'
		$(selector).removeClass('success danger clickable')
		$(selector).addClass('warning')
	} else if (message.body.status == 'PRC') {
		message.body.status = 'Game in process'
		$(selector).removeClass('success warning clickable')
		$(selector).addClass('danger')
	}

	lobbies_manager.update(message.body)
})

lobbies_emitter.subscribe('delete', message => {
	selector_pattern = '[environment="lobbies"] [item-id="$"]'
	selector = selector_pattern.replace('$', message.body.id)

	lobbies_manager.delete(message.body)
})