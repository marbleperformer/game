from django.conf.urls import url
import lobbies.views as views

app_name = 'lobbies'

urlpatterns = [
	url(r'^$', views.LobbyListView.as_view(), name='index'),
	url(r'^detail/(?P<pk>[0-9]+)$', views.LobbyDetailView.as_view(), name='detail'),
	url(r'^create$', views.LobbyCreateView.as_view(), name='create'),
	url(r'^playground/(?P<pk>[0-9]+)$', views.PlaygroundView.as_view(), name='playground')
	# url(r'^update/(?P<pk>[0-9]+)$', views.LobbyUpdateView.as_view(), name='update'),
	# url(r'^delete/(?P<pk>[0-9]+)$', views.LobbyDeleteView.as_view(), name='delete'),
]
