from django.contrib import admin

from .models import Lobby, Action, Hint, Turn

class LobbyAdmin(admin.ModelAdmin):
	list_display = ['name', 'owner', 'created', 'closed',]
	list_filter = ['owner', 'created', 'closed',]
	search_fields = ['name', 'owner',]

	filter_horizontal = ['players']

	fieldsets = (
		(None, {'fields':('name', 'status', )}), 
		('Relarions info', {'fields':('owner', 'winner', 'players')}),
		('Date info', {'fields':('closed',)}),
	)

class ActionAdmin(admin.ModelAdmin):
	list_display = ['player', 'lobby', 'created']
	list_filter = ['player', 'lobby', 'created']
	search_fields = ['palayer',]

	fields = ('player', 'lobby',)

class HintAdmin(admin.ModelAdmin):
	list_display = ['player', 'lobby', 'created']
	list_filter = ['player', 'lobby', 'created']
	search_fields = ['palayer',]

	fields = ('player', 'lobby',)

class TurnAdmin(admin.ModelAdmin):
	list_display = ['player', 'lobby', 'created']
	list_filter = ['player', 'lobby', 'created']
	search_fields = ['palayer',]

	fieldsets = (
		(None, {'fields':('figure',)}), 
		('Relarions info', {'fields':('player', 'lobby')}),
	)

admin.site.register(Lobby, LobbyAdmin)
# admin.site.register(Session, SessionAdmin)
admin.site.register(Action, ActionAdmin)
admin.site.register(Hint, HintAdmin)
admin.site.register(Turn, TurnAdmin)
